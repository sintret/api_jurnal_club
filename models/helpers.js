const conn = require('./database')
var Util = require('./../components/Util')

var helpers = {}

helpers.insertContact = function (post) {

    return new Promise(function (resolve, reject) {
        var phone = Util.phoneFixed(post.phone);
        var userId = post.userId
        console.log(phone)

        var query = conn.query('select * from phone where phone = ? and userId = ? ', [phone, userId], function (error, rows, fields) {
            if (error) {
                resolve(error)
            }

            console.log("Jumlah phone : " + rows.length)
            if (rows.length == 0) {

                conn.query('insert into phone SET ?', post, function (err, results, field) {

                    if (err) {
                        console.log(err)
                        reject(err)
                    }
                    console.log(results)
                    resolve(results)

                })
            } else {
                resolve({status: 1})
            }
        });
    })
}

module.exports = helpers