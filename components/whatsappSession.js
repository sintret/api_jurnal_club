/**
 * Created by sintret dev on 11/23/2021.
 */
var connection = require("./../models/database");
var sendSocket = require("./sendSocket");
const { Client, List, Buttons, Location } = require('whatsapp-web.js');
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');


async function me(req,res,next) {
    var users = await connection.query("select * from `user` where wabrowser != ''");
    //console.log(users);
    users.forEach(function (userObj) {
        var token = userObj.token;
        console.log(userObj.username + " : "+token);
        var sessionCfg = Util.isJsonString(userObj.wasession) ? JSON.parse(userObj.wasession) : "";
        if(sessionCfg) {
            var webhook = userObj.webhook;
            var args = [
                '--no-sandbox',
                //'--disable-setuid-sandbox',
                //'--use-gl=egl',
              /*  '--disable-dev-shm-usage',
                '--disable-accelerated-2d-canvas',
                '--no-first-run',
                '--no-zygote',
                '--single-process',
                '--disable-gpu'*/
            ];
            console.log(args);
            var path = '/usr/bin/google-chrome-stable';
            console.log(path);
            if(!waClients[token]) {
                //waClients[token] = new Client({headless: true, clientId: token, executablePath: path , timeout: 9000,args: args, session: sessionCfg});
                //waClients[token] = new Client({headless: true, args: ['--no-sandbox'],session: sessionCfg});
                waClients[token] = new Client({
                    session: sessionCfg,
                    //qrTimeoutMs: 120000,
                    //authTimeoutMs: 120000,
                    //restartOnAuthFail: true,
                    //takeoverOnConflict: true
                    //takeoverTimeoutMs: 5000
                    puppeteer: {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-extensions']}
                });
                waClients[token].initialize();
                waClients[token].on('ready', () => {
                    console.log(`Client ${token} is ready!`);
                    sendSocket.stats({token:token,type:"qrcodesuccess",message:"Ready"}, function (data) {
                        console.log(data)
                    });

                    let info = waClients[token].info;
                    let msg = `
                    *Connection info*
                    User name: ${info.pushname}
                    My number: ${info.me.user}
                    Platform: ${info.platform}
                    WhatsApp version: ${info.phone.wa_version}`;
                    console.log(msg);

                });
                waClients[token].on('message', async msg => {
                    console.log('MESSAGE RECEIVED', msg);
                    if(webhook) {
                        let chat = await msg.getChat();
                        var obj = {
                            token : token,
                            chat : chat,
                            msg: msg
                        }

                        const options = {
                            method: 'POST',
                            headers: {'content-type': 'application/x-www-form-urlencoded'},
                            data: qs.stringify(obj),
                            url: webhook
                        };
                        //console.log(JSON.stringify(chat));
                        try {
                            var cb = await axios(options);
                            console.log("axios sudah..");
                            var replyData = cb.data;
                            console.log(typeof replyData);
                            console.log("replyData..");
                            console.log(replyData);
                            if(replyData.isReply) {
                                msg.reply(replyData.message);
                            }
                        } catch (error) {
                            console.log(error);
                        }
                    }

                    if (msg.body === '!ping') {
                        // Send a new message as a reply to the current one
                        msg.reply('pong');
                    }  else if (msg.body === '!ping') {
                        // Send a new message to the same chat
                        waClients[token].sendMessage(msg.from, 'pong');

                    } else if (msg.body.startsWith('!sendto ')) {
                        // Direct send a new message to specific id
                        let number = msg.body.split(' ')[1];
                        let messageIndex = msg.body.indexOf(number) + number.length;
                        let message = msg.body.slice(messageIndex, msg.body.length);
                        number = number.includes('@c.us') ? number : `${number}@c.us`;
                        let chat = await msg.getChat();
                        chat.sendSeen();
                        waClients[token].sendMessage(number, message);

                    } else if (msg.body.startsWith('!subject ')) {
                        // Change the group subject
                        let chat = await msg.getChat();
                        if (chat.isGroup) {
                            let newSubject = msg.body.slice(9);
                            chat.setSubject(newSubject);
                        } else {
                            msg.reply('This command can only be used in a group!');
                        }
                    } else if (msg.body.startsWith('!echo ')) {
                        // Replies with the same message
                        msg.reply(msg.body.slice(6));
                    } else if (msg.body.startsWith('!desc ')) {
                        // Change the group description
                        let chat = await msg.getChat();
                        if (chat.isGroup) {
                            let newDescription = msg.body.slice(6);
                            chat.setDescription(newDescription);
                        } else {
                            msg.reply('This command can only be used in a group!');
                        }
                    } else if (msg.body === '!leave') {
                        // Leave the group
                        let chat = await msg.getChat();
                        if (chat.isGroup) {
                            chat.leave();
                        } else {
                            msg.reply('This command can only be used in a group!');
                        }
                    } else if (msg.body.startsWith('!join ')) {
                        const inviteCode = msg.body.split(' ')[1];
                        try {
                            await waClients[token].acceptInvite(inviteCode);
                            msg.reply('Joined the group!');
                        } catch (e) {
                            msg.reply('That invite code seems to be invalid.');
                        }
                    } else if (msg.body === '!groupinfo') {
                        let chat = await msg.getChat();
                        if (chat.isGroup) {
                            var owner = chat.owner;
                            var ownerUser = "Not Found!!!!";
                            if(owner) {
                                ownerUser = owner.user
                            }
                            msg.reply(`
                *Group Details*
                Name: ${chat.name}
                Description: ${chat.description}
                Created At: ${chat.createdAt.toString()}
                Created By: ${ownerUser}
                Participant count: ${chat.participants.length}
            `);
                        } else {
                            msg.reply('This command can only be used in a group!');
                        }
                    } else if (msg.body === '!chats') {
                        const chats = await waClients[token].getChats();
                        waClients[token].sendMessage(msg.from, `The bot has ${chats.length} chats open.`);
                    } else if (msg.body === '!info') {
                        let info = waClients[token].info;
                        waClients[token].sendMessage(msg.from, `
            *Connection info*
            User name: ${info.pushname}
            My number: ${info.me.user}
            Platform: ${info.platform}
            WhatsApp version: ${info.phone.wa_version}
        `);
                    } else if (msg.body === '!mediainfo' && msg.hasMedia) {
                        const attachmentData = await msg.downloadMedia();
                        msg.reply(`
            *Media info*
            MimeType: ${attachmentData.mimetype}
            Filename: ${attachmentData.filename}
            Data (length): ${attachmentData.data.length}
        `);
                    } else if (msg.body === '!quoteinfo' && msg.hasQuotedMsg) {
                        const quotedMsg = await msg.getQuotedMessage();

                        quotedMsg.reply(`
            ID: ${quotedMsg.id._serialized}
            Type: ${quotedMsg.type}
            Author: ${quotedMsg.author || quotedMsg.from}
            Timestamp: ${quotedMsg.timestamp}
            Has Media? ${quotedMsg.hasMedia}
        `);
                    } else if (msg.body === '!resendmedia' && msg.hasQuotedMsg) {
                        const quotedMsg = await msg.getQuotedMessage();
                        if (quotedMsg.hasMedia) {
                            const attachmentData = await quotedMsg.downloadMedia();
                            waClients[token].sendMessage(msg.from, attachmentData, { caption: 'Here\'s your requested media.' });
                        }
                    } else if (msg.body === '!sticker' && msg.hasMedia) {
                        const attachmentData = await msg.downloadMedia();
                        waClients[token].sendMessage(msg.from, attachmentData, { sendMediaAsSticker: true });
                    } else if (msg.body === '!location') {
                        msg.reply(new Location(37.422, -122.084, 'Googleplex\nGoogle Headquarters'));
                    } else if (msg.location) {
                        msg.reply(msg.location);
                    } else if (msg.body.startsWith('!status ')) {
                        const newStatus = msg.body.split(' ')[1];
                        await waClients[token].setStatus(newStatus);
                        msg.reply(`Status was updated to *${newStatus}*`);
                    } else if (msg.body === '!mention') {
                        const contact = await msg.getContact();
                        const chat = await msg.getChat();
                        chat.sendMessage(`Hi @${contact.number}!`, {
                            mentions: [contact]
                        });
                    } else if (msg.body === '!delete') {
                        if (msg.hasQuotedMsg) {
                            const quotedMsg = await msg.getQuotedMessage();
                            if (quotedMsg.fromMe) {
                                quotedMsg.delete(true);
                            } else {
                                msg.reply('I can only delete my own messages');
                            }
                        }
                    } else if (msg.body === '!pin') {
                        const chat = await msg.getChat();
                        await chat.pin();
                    } else if (msg.body === '!archive') {
                        const chat = await msg.getChat();
                        await chat.archive();
                    } else if (msg.body === '!mute') {
                        const chat = await msg.getChat();
                        // mute the chat for 20 seconds
                        const unmuteDate = new Date();
                        unmuteDate.setSeconds(unmuteDate.getSeconds() + 20);
                        await chat.mute(unmuteDate);
                    } else if (msg.body === '!typing') {
                        const chat = await msg.getChat();
                        // simulates typing in the chat
                        chat.sendStateTyping();
                    } else if (msg.body === '!recording') {
                        const chat = await msg.getChat();
                        // simulates recording audio in the chat
                        chat.sendStateRecording();
                    } else if (msg.body === '!clearstate') {
                        const chat = await msg.getChat();
                        // stops typing or recording in the chat
                        chat.clearState();
                    } else if (msg.body === '!jumpto') {
                        if (msg.hasQuotedMsg) {
                            const quotedMsg = await msg.getQuotedMessage();
                            waClients[token].interface.openChatWindowAt(quotedMsg.id._serialized);
                        }
                    } else if (msg.body === '!buttons') {
                        let button = new Buttons('Button body',[{body:'bt1'},{body:'bt2'},{body:'bt3'}],'title','footer');
                        waClients[token].sendMessage(msg.from, button);
                    } else if (msg.body === '!list') {
                        let sections = [{title:'sectionTitle',rows:[{title:'ListItem1', description: 'desc'},{title:'ListItem2'}]}];
                        let list = new List('List body','btnText',sections,'Title','footer');
                        waClients[token].sendMessage(msg.from, list);
                    }
                });
            }
        }
    })

    next();
}

module.exports = me;