const qrcode = require('qrcode-terminal');
const { Client } = require('whatsapp-web.js');
var conn = require('./../models/database');
var QRCode = require('qrcode')


function whatsapp(req,res, sessionCfg) {
    var client;
    if(!sessionCfg) {
        console.log("new client")
        client = new Client();
        client.on('qr', qr => {
            QRCode.toDataURL(qr, function (err, url) {
                //console.log(url);
                var data = {
                    qrcode: url
                }
                conn.query("update user set ? where id = ?", [data, req.session.user.id]);
            });
        });
    } else {
        console.log("existing client");
        client = new Client({ puppeteer: { headless: true }, session: JSON.parse(sessionCfg)});
    }



    client.on('ready', () => {
        console.log('Client is ready!');
    });

    client.on('authenticated', (session) => {
        console.log('AUTHENTICATED', session);
        sessionCfg=session;
        var data = {
            wasession:JSON.stringify(sessionCfg),
            qrcode: ""
        }

         conn.query("update user set ? where id = ?", [data, req.session.user.id]);


        /*fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
         if (err) {
         console.error(err);
         }
         });*/
        client.qrIcon = "You have already Login";
    });

    client.initialize();
    return client;
}

module.exports = whatsapp;