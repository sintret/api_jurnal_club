/*
terbaru 2022
from <String>
    - bisa dari group from : 120363023801393207@g.us
    - non group 6281575068530@s.whatsapp.net
isGroup<bool> true false;
sender : 6281575068530@s.whatsapp.net
senderNumber : 6281575068530
botNumber : 6281511880118
type : conversation
quoted : [object Object]
body : ini apa
 */
const {
    default: makeWASocket,
    useSingleFileAuthState,
    DisconnectReason,
    getContentType
} = require('@adiwajshing/baileys')
var fs = require('fs')
var QRCode = require('qrcode');
var connection = require("./../models/database");
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');
var sendSocket = require("./sendSocket");
const { Boom } = require("@hapi/boom");
const prefix = '.'
const ownerNumber = ['595995660558']

const WAConnect = (userObj) => {
    if(!userObj.token) return;
    waDoing = 1;
    var token = userObj.token || "";
    var webhook = userObj.webhook || "";
    var botNumber="";
    //return new Promise(function (resolve,reject) {
        var dirSession = `${dirRoot}/public/sessions/`;
        var sessionFile = `${token}.json`;
        const {state, saveState} = useSingleFileAuthState(dirSession + sessionFile)
        const conn = makeWASocket({
            //logger: P({ level: 'silent' }),
            printQRInTerminal: true,
            auth: state,
            /*connectTimeoutMs: conn.CONNECTION_TIMEOUT,
            defaultQueryTimeoutMs: conn.CONNECTION_TIMEOUT,
            keepAliveIntervalMs: conn.CONNECTION_TIMEOUT,*/
        })
        var isSave =false;
        var isSend=false;

        conn.ev.on('connection.update', (update) => {
            const {connection, lastDisconnect, qr} = update
            let reason;
            if(qr) {
                console.log(qr);
                QRCode.toDataURL(qr,  function (err, url) {
                    if (err) {
                        console.log("error", err);
                    }
                    console.log(url);
                    sendSocket.stats({token: token, type: "qrcode", message: url}, function (data) {
                        //console.log(data)
                    });
                });
            }  else if (connection === 'close') {
                const statusCode = lastDisconnect.error ? lastDisconnect.error?.output?.statusCode : 0;
                if (lastDisconnect.error.output.statusCode !== DisconnectReason.loggedOut) {
                    WAConnect(userObj)
                } else {
                    const DR = DisconnectReason
                    if (statusCode === DR.badSession) {
                        console.log(`Bad session ${sessionFile} Please rescan QR Code.`);
                        conn.logout();
                        try {
                            fs.unlinkSync(dirSession + sessionFile)
                            delete waClients[token];
                        } catch (err) {
                            console.error(err)
                        }
                        return
                    }
                    if (statusCode === DR.connectionClosed) {
                        console.log("Connection closed. Reconnecting...");
                        WAConnect(userObj);
                        return
                    }
                    if (statusCode === DR.connectionLost) {
                        console.log("lost with the server. Trying to reconnect...");
                        waObject[token] = {
                            wasession:1,
                            botNumber:botNumber||""
                        }
                        WAConnect(userObj);
                        return
                    }
                    if (statusCode === DR.connectionReplaced) {
                        console.log("Current session replaced by the new one opened. Please close this session first.");
                        try {
                            fs.unlinkSync(dirSession + sessionFile)
                            delete waClients[token];
                        } catch (err) {
                            console.error(err)
                        }
                        return
                    }
                    if (statusCode === DR.restartRequired) {
                        console.log("Kingdom necessary. restarting...");
                        WAConnect(userObj);
                        return
                    }
                    if (statusCode === DR.timedOut) {
                        console.log("Connection timed out, Reconnecting...");
                        WAConnect(userObj);
                        return
                    }
                }


            } else if (connection === 'open') {
                console.log('Bot Connected')
                if(waClients.hasOwnProperty(token)){
                    console.log("sudah ada waclients")
                } else {
                    console.log("Bot konek tapi blom ada waclients")
                }
                waClients[token]=conn;
                sendSocket.stats({token: token, type: "qrcodesuccess", message: "Ready"}, function (data) {
                        //console.log(data)
                    });
                isSave =true;
                isSend =true;
            }
        })

        conn.ev.on('creds.update', saveState)

        var hasMessage = false;
        conn.ev.on('messages.upsert', async(mek) => {
            try {
                waDoing=0;
                isSave = true;
                hasMessage=true;
                console.log(JSON.stringify(mek))
                mek = mek.messages[0]
                if (!mek.message) return
                var obj = {}
                obj.messages=mek;

                mek.message = (getContentType(mek.message) === 'ephemeralMessage') ? mek.message.ephemeralMessage.message : mek.message
                if (mek.key && mek.key.remoteJid === 'status@broadcast') return
                const type = getContentType(mek.message)

                console.log(`type : ${type}`)
                const content = JSON.stringify(mek.message)
                const from = mek.key.remoteJid
                console.log(`from : ${from}`)

                const quoted = type == 'extendedTextMessage' && mek.message.extendedTextMessage.contextInfo != null ? mek.message.extendedTextMessage.contextInfo.quotedMessage || [] : []
                console.log(`quoted : ${JSON.stringify(quoted)}`)

                const body = (type === 'conversation') ? mek.message.conversation : (type === 'extendedTextMessage') ? mek.message.extendedTextMessage.text : (type == 'imageMessage') && mek.message.imageMessage.caption ? mek.message.imageMessage.caption : (type == 'videoMessage') && mek.message.videoMessage.caption ? mek.message.videoMessage.caption : ''
                console.log(`body : ${body}`)

                const isCmd = body.startsWith(prefix)
                const command = isCmd ? body.slice(prefix.length).trim().split(' ').shift().toLowerCase() : ''
                console.log(`command : ${command}`)

                const args = body.trim().split(/ +/).slice(1)
                const q = args.join(' ')
                const isGroup = from.endsWith('@g.us')
                console.log(`isGroup : ${isGroup}`)

                const sender = mek.key.fromMe ? (conn.user.id.split(':')[0]+'@s.whatsapp.net' || conn.user.id) : (mek.key.participant || mek.key.remoteJid)
                console.log(`sender : ${sender}`)

                const senderNumber = sender.split('@')[0]
                console.log(`senderNumber : ${senderNumber}`)

                botNumber = conn.user.id.split(':')[0]
                console.log(`botNumber : ${botNumber}`)

                const pushname = mek.pushName || 'No Name'
                console.log(`pushname : ${pushname}`)
                const isMe = botNumber.includes(senderNumber)
                console.log(`isMe : ${isMe}`)

                const isOwner = ownerNumber.includes(senderNumber) || isMe
                console.log(`isOwner : ${isOwner}`)

                const reply = async(teks) => {
                    await conn.sendMessage(from, { text: teks }, { quoted: mek })
                }

                switch (command) {
                    case 'hola':
                        reply(`Hola ${pushname} como estas? :D`)
                        break;

                }
                waClients[token]=conn;
                if(webhook) {
                    obj.from = from;
                    obj.type = type;
                    obj.isGroup = isGroup;
                    obj.quoted = quoted;
                    obj.body = body;
                    obj.sender = sender;
                    obj.senderNumber=senderNumber;
                    obj.botNumber=botNumber;
                    obj.pushname=pushname;
                    obj.isMe=isMe;
                    obj.isOwner=isOwner;

                    postData(webhook, obj);
                }

                var count = waCount[token] || 1;
                waCount[token] += count;
                waObject[token] = {
                    wasession:1,
                    botNumber:botNumber||"",
                    count:waCount[token]
                }
            } catch (e) {
                const isError = String(e)
                console.log(isError)
            }
        })
    //})
}



var postData = (url, body) => {
    const options = {
        method: 'POST',
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        data: qs.stringify(body),
        url: url
    }
    axios(options).then(function (response) {
        //console.log(response);
    })
        .catch(function (error) {
            //console.log(error);
        });
    //var cb = await axios(options);
    console.log("send to " + url);
}


module.exports = WAConnect;