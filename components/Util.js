const moment = require('moment');
const path = require('path');
const saltRounds = 10;
var randomstring = require('randomstring');


//you must first call storage.initSync
var Util = {}
Util.excelSequence = function () {
    return [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
        'EA', 'EB', 'EC', 'EE', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
    ];
}

Util.capitalizeFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

Util.asyncWrap = (fn) => {
    return (req, res, next) => {
        fn(req, res, next).catch(next);
    }
}

Util.now = function () {
    return moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
}

Util.yesterday = function () {
    var t = moment().subtract(1, 'days');
    return moment(t).format("YYYY-MM-DD HH:mm:ss");
}

Util.nowShort = function () {
    return moment(new Date()).format("YYYY-MM-DD");
}

Util.nowShortYesterday = function () {
    var t = moment().subtract(1, 'days');
    return moment(t).format("YYYY-MM-DD");
}


Util.tableArray = function (arr) {
    var r = [];
    var tables = arr[0];

    for (var i = 0; i < tables.length; i++) {
        for (var obj in tables[i]) {
            r.push(tables[i][obj]);
        }
    }

    return r;
}

Util.escapeRegExp = function (str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

Util.validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/*
 Replace All like str_replace in PHP
 example : Util.replaceAll("abd","a","")
 */
Util.replaceAll = function (str, find, replace) {

    var t = str;
    var arr = [];
    if (Array.isArray(find)) {
        for (var i = 0; i < find.length; i++) {
            if (t.indexOf(find[i]) > -1) {
                t = t.replace(new RegExp(Util.escapeRegExp(find[i]), 'g'), replace);
            }
        }

    } else {
        t = str.replace(new RegExp(Util.escapeRegExp(find), 'g'), replace);
    }

    return t;
}


Util.phoneFixed = function (str) {

    var ret = ''
    str = Util.replaceAll(str, ' ', '')
    var phone = str.trim()
    phone = Util.replaceAll(phone, '-', '')
    var first = phone.charAt(0)

    if (first == '') {
        ret = '';
    } else if (first == '+') {
        ret = phone;
    } else if (first == '0') {
        ret = '+62' + phone.replace('0', '');
    } else {
        ret = '+' + phone
    }

    return ret;
}

Util.jsonSuccess = function () {
    var json = {type: 'success', status: 1, title: 'Success', message: 'Data saved...'}

    return json;
}

Util.jsonError = function (path, message) {
    var json = {}
    json.status = 0;
    json.title = 'Error';
    var obj = {errors: [{path: path, message: message}]}
    json.data = obj;

    return json;
}

Util.extractObj = function (params) {

    var newKey = {}
    Object.keys(params).forEach(function (key, index) {
        var arr = [];
        for (var k in params[key]) {
            arr.push(k);
        }
        newKey[key] = arr;
    });

    return newKey;
}

Util.getField = function (arr, id, field) {
    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i]['id'] == id) {
                return arr[i][field];
            }
        }
    }
}

Util.remove = function (arr, id, field) {
    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][field] == id) {
                arr.splice(i, 1)
            }
        }
    }

    return arr;
}

Util.getUserObject = function (arr, id) {
    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i]['id'] == id) {
                return arr[i];
            }
        }
    }
}

Util.generateUnique = function (length, charset) {

    var random = Util.generate(length, charset);
    var uid = (new Date().valueOf()).toString(36)

    return uid + random;
}


Util.generate = function (length, charset) {

    length = length || 50;
    //alphanumeric - [0-9 a-z A-Z] alphabetic - [a-z A-Z] numeric - [0-9]  hex - [0-9 a-f] custom - any given characters
    charset = charset || "alphanumeric";

    return randomstring.generate({
        length: length,
        charset: charset
    });

}

Util.getFiles = function (dir, files_) {
    files_ = files_ || [];

    if (fs.statSync(dir).isDirectory()) {
        var files = fs.readdirSync(dir);
        for (var i in files) {
            var name = dir + '/' + files[i];
            //var name = path.join(dir,files[i]);
            if (fs.statSync(name).isDirectory()) {
                Util.getFiles(name, files_);
            } else {
                files_.push(name);
            }
        }
    }

    return files_;
}

//various type


Util.random = function (length) {
    length = length || 5;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


Util.typePrint = {
    register: '{"paper_size":"F4","paper_size_width":"215","paper_size_height":"330","padding_top":"8","padding_right":"8","padding_bottom":"8","padding_left":"8","border":"1","font":"0","font_size":"10","header":"SURAT PERINTAH KERJA","header_font":"0","header_font_size":"26"}',
    estimation: '{"paper_size":"F4","paper_size_width":"215","paper_size_height":"330","padding_top":"8","padding_right":"8","padding_bottom":"8","padding_left":"8","border":"1","font":"0","font_size":"12","header":"ESTIMASI BIAYA PERBAIKAN","header_font":"0","header_font_size":"18"}',
    invoice: '{"paper_size":"A5","paper_size_width":"148","paper_size_height":"210","padding_top":"8","padding_right":"8","padding_bottom":"8","padding_left":"8","border":"1","font":"0","font_size":"12","header":"INVOICE","header_font":"0","header_font_size":"18"}',
    currency: '{"symbol":"Rp","name":"Rupiah","thousand":"."}'
}

Util.whitelist = function () {
    return ['www', 'app', 'my', 'sintret', 'https', 'https'];
}

Util.convertDate = function (d) {
    d = d.trim()
    var myarr = d.split(" ");

    return myarr[2] + '-' + Util.monthConvert(myarr[1]) + '-' + myarr[0];
}

Util.month = function () {
    return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
}

Util.monthConvert = function (month) {

    var t = Util.month();
    var index = t.indexOf(month) + 1;
    if (index < 10) {
        return '0' + index
    } else {
        return index;
    }
}

Util.paymentMethods = function () {
    //BK / VC / MY / CK / BT
    return [
        {value: 'VC', name: 'Credit Card'},
        {value: 'BK', name: 'BCA KlikPay'},
        {value: 'MY', name: 'Mandiri Clickpay'},
        {value: 'BT', name: 'Permata Bank Virtual Account'},
        {value: 'B1', name: 'CIMB Niaga Virtual Account'},
        {value: 'A1', name: 'ATM Bersama'},
        {value: 'I1', name: 'BNI Virtual Account'},
        {value: 'I2', name: 'Danamon Virtual Account'},
        {value: 'VA', name: 'Maybank Virtual Account'},
        {value: 'CK', name: 'CIMB Click'},
    ]
}

Util.getField = function (arr, key, keyName, field) {
    if (arr.length > 0) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][key] == keyName) {
                return arr[i][field];
            }
        }
    }
}


Util.operatorPhone = function (str) {
    var code = '';
    var op = str.substring(0, 4);

    if (op == '0811' || op == '0812' || op == '0813' || op == '0852' || op == '0853' || op == '0821' || op == '0822' || op == '0823') {
        code = 'S';
    } else if (op == '0814' || op == '0815' || op == '0816' || op == '0855' || op == '0856' || op == '0857' || op == '0858') {
        code = 'I';
    } else if (op == '0817' || op == '0818' || op == '0819' || op == '0859' || op == '0877' || op == '0878') {
        code = 'X';
    } else if (op == '0831' || op == '0832' || op == '0838') {
        code = 'AX';
    } else if (op == '0896' || op == '0897' || op == '0898' || op == '0899') {
        code = 'T';
    } else if (op == '0888' || op == '0889' || op == '0881' || op == '0882' || op == '0883' || op == '0884' || op == '0885' || op == '0886' || op == '0887' || op == '0887' || op == '0888') {
        code = 'SM';
    }

    return code

}

Util.hargaPhone = function () {
    return [5, 10, 20, 25, 50, 100]
}


Util.randomRange = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Util.downloadImage = function (url, filename) {

    return new Promise(function (resolve) {
        var writeFileStream = fs.createWriteStream(filename)

        request(url).pipe(writeFileStream).on('close', function () {
            console.log(url, 'saved to', filename)
            resolve(filename)
        });
    })
}

Util.imageUrlText = function (url) {

    var unique = Util.generateUnique(10);
    //var filename = '/var/www/image/' + unique + '.png';

    var filename = 'C:/edil/' + unique + '.png';

    var writeFile = fs.createWriteStream(filename);

    return new Promise(function (resolve) {

        request(url).pipe(writeFile).on('close', function () {
            console.log(url, 'saved to', filename)
            Tesseract.recognize(filename)
                .progress(function (p) {
                    console.log('progress', p)
                })
                .catch(err => console.error(err))
                .then(function (result) {
                    console.log(result.text)
                    resolve(result.text);
                })
        });

    });
}

Util.isNull = function (str) {
    if (str == "") return true;
    var regu = "^[ ]+$";
    var re = new RegExp(regu);
    return re.test(str);
}

Util.isJson = function (text) {
 return true;
}

Util.isJsonxx = function (text) {
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
        replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
        replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

        //the json is ok
        return true;
    }else{
        //the json is not ok
        return false;
    }
}

Util.post = function (options) {
    options.method = 'POST';
    return new Promise(function (resolve, reject) {
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            resolve(body)
        });
    })
}

Util.isJsonString = function(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

module.exports = Util;