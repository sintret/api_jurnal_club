var Util = require('./Util');
var qr = require('./scanqr')
var qr2 = require('./qr')
var connection = require("./../models/database");
var axios = require('axios');
var qs = require('qs');
const {MessageType, MessageMedia, Presence, Mimetype} = require('@adiwajshing/baileys');
var fs = require('fs');
var config = require('./../config/config');
var url = 'https://api.jurnal.club';

/*
 const sentMsg  = await myclient[device].sendMessage (jid, message, MessageType.Text, { quoted: quoted } ); 
 */

async function send(body) {
    console.log(body)
    var json = Util.jsonSuccess("Success to send");
    var token = body.token;
    var to = body.to;
    var vcard = body.vcard || "";
    var mime = body.mime || "";
    if (to.indexOf("@s.whatsapp.net") > -1) {
    } else {
        if (to.indexOf("@g.us") > -1) {
        } else {
            to = to + "@s.whatsapp.net"
        }
    }
    var text = body.text || "";
    var url = body.url || "";
    var image = body.image || "";
    var type = body.type;
    var name = body.name || "";
    var reply = body.message;
    var mimes = {};
    var options = {};
    var quoted = "";

    try {
        var isReply = body.isReply ? true : false;
        if (isReply) {
            quoted = waMessages[body.phone] || null;
            options = {quoted: quoted}
        }

        switch (type) {
            case "text" :
                //await waClients[token].sendMessage(to, text, MessageType.text, options);
                await waClients[token].sendMessage(to, { text: teks });
                break;
            case "image" :
                if (url) {
                    console.log("image has url");
                    if (mime) {
                        mimes.mimetype = mime;
                    } else {
                        var content = await axios.get(url);
                        mime = content.headers["content-type"];
                        console.log(mime);
                        mimes.mimetype = mime;
                    }
                    if (text) {
                        mimes.caption = text;
                    }
                    if (isReply) {
                        mimes.quoted = quoted;
                    }
                    //var mime = mimeTypes.loo
                    await waClients[token].sendMessage(
                        to,
                        {url: url}, // send directly from remote url!
                        MessageType.image,
                        mimes
                    )
                } else {
                    mimes.mimetype = mime;
                    if (text) {
                        mimes.caption = text;
                    }
                    if (isReply) {
                        mimes.quoted = quoted;
                    }
                    var ext = mime.split("/")[1] || "jpg";
                    if (ext == "jpeg") ext = "jpg";
                    const d = new Date();
                    var filename = d.getTime() + "." + ext;
                    fs.writeFileSync(dirRoot + "/public/downloads/" + filename, image, 'base64');
                    var url = "https://api.jurnal.club/downloads/" + filename;
                    await waClients[token].sendMessage(
                        to,
                        {url: url}, // send directly from remote url!
                        MessageType.image,
                        mimes
                    )
                }
                break;

            case "video" :
                mimes.mimetype = Mimetype.gif;
                if (text) {
                    mimes.caption = text;
                }
                await waClients[token].sendMessage(to, {url: url}, MessageType.video, mimes);
                break;

            case "extendedText" :
                await waClients[token].sendMessage(to, text, MessageType.extendedText, options);
                break;

            case "sticker" :
                /*const sticker = await new Sticker(url, {
                 pack: 'Andy Laser Pack', // The pack name
                 author: 'Me', // The author name
                 type: StickerTypes.FULL, // The sticker type
                 categories: ['Robot', 'Story'], // The sticker category
                 id: '12345', // The sticker id
                 quality: 50, // The quality of the output file
                 background: '#000000' // The sticker background color (only for full stickers)
                 })*/

                //const buffer = await sticker.toBuffer() // convert to buffer
                //await sticker.toFile('sticker2.webp')


                /* const buffer = await new Sticker(url)
                 .setPack('My Pack')
                 .setAuthor('Me')
                 .setType(StickerTypes.FULL)
                 .setCategories(['🤩', '🎉'])
                 .setId('12345')
                 .setBackground('#000000')
                 .setQuality(50)
                 .toBuffer() */

                //await waClients[token].sendMessage(to, sticker.toBuffer(), MessageType.sticker,{ mimetype: "image/webp"} )

                //await waClients[token].sendMessage(to, {sticker: await sticker.toBuffer(), mimetype: "image/webp"});
                break;

            case "vcard" :
                await waClients[token].sendMessage(to, {displayname: name, vcard: text}, MessageType.contact)
                break;

            case "location" :
                var location = text.split(',');
                await waClients[token].sendMessage(to, {
                    degreesLatitude: location[0],
                    degreesLongitude: location[1]
                }, MessageType.location, options);
                break;


            case "list" :
                // send a list message!
                console.log(body)
                const rows = [
                    {title: 'Row 1', description: "@6287846943631", rowId: "rowid1"},
                    {title: 'Row 2', description: "Hello it's description 2", rowId: "rowid2"}
                ]
                var sections = [{title: "Approvers", rows: rows}, {title: "Knowings", rows: rows}];
                console.log(typeof body.sections)
                if (body.sections) {
                    if (typeof body.sections === 'string') {
                        sections = JSON.parse(body.sections)
                    } else {
                        sections = body.section;
                    }
                }
                var buttonText = body.buttonText || 'Click Me, see details';
                const button = {
                    buttonText: buttonText,
                    description: text,
                    sections: sections,
                    listType: 1
                }

                await waClients[token].sendMessage(to, button, MessageType.listMessage, options)
                break;

            case "button" :
                // send a buttons message!
                const buttons1 = [
                    {buttonId: 'id1', buttonText: {displayText: 'Button 1'}, type: 2},
                    {buttonId: 'id2', buttonText: {displayText: 'Button 2'}, type: 1}
                ]
                var buttons = body.buttons ? JSON.parse(body.buttons) : buttons1;
                var footerText = body.footerText || "Please post footerText";
                const buttonMessage = {
                    contentText: text,
                    footerText: footerText,
                    buttons: buttons,
                    headerType: 1
                }
                await waClients[token].sendMessage(to, buttonMessage, MessageType.buttonsMessage, options)
                break;

            case "typing" :
                console.log("typing...")
                await waClients[token].updatePresence(to, Presence.composing);
                break;

            case "profile" :
                const ppUrl = await waClients[token].getProfilePicture(text) // leave empty to get your own
                var response = await axios({
                    method: "get",
                    url: ppUrl,
                    responseType: "stream"
                });
                var filename = dirRoot + "/public/pp/" + text + ".jpg"
                response.data.pipe(fs.createWriteStream(filename));
                var url = "https://api.jurnal.club/pp/" + text + ".jpg";
                const status = await waClients[token].getStatus(text);// leave empty to get your own status
                body.type = "image";
                body.url = url;
                body.text = status.status;
                body.mime = "image/jpeg";
                console.log(body);
                await send(body);
                break;

            case "groupcode" :
                if (to.includes("@g.us")) {
                    const response = await waClients[token].revokeInvite(to)
                    console.log("group code: " + response.code);
                    await waClients[token].sendMessage(to, response.code, MessageType.text);
                }
                break;

            case "changeprofile" :
                if (body.message) {
                    var m = JSON.parse(body.message);
                    const buffer = await waClients[token].downloadMediaMessage(m) // to decrypt & use as a buffer
                    const savedFilename = await waClients[token].downloadAndSaveMediaMessage(m) // to decrypt & save to file
                    console.log(" sent media, saved at: " + savedFilename)
                    await waClients[token].sendMessage(to, savedFilename, MessageType.text);

                    /*

                     await waClients[token].sendMessage(to, "Update photo success", MessageType.text);*/
                }

                break;

            case "invite" :
                if (text.includes("@g.us")) {
                    var subject = "";
                    var textGroup = "";
                    const metadata = await waClients[token].groupMetadata(text);
                    if (metadata.subject) subject = metadata.subject;
                    textGroup += `
                *Group Details*
                Phone : ${metadata.id}
                Name: ${subject}
                Description: ${metadata.desc}
                Participant count: ${metadata.participants.length} Members
                `;


                    const code = await waClients[token].groupInviteCode(to);
                    textGroup += `Follow this link to join my WhatsApp group: \r\nhttps://chat.whatsapp.com/${code}`
                    //await waClients[token].sendMessage(to, textGroup, MessageType.text);

                    body.type = "text";
                    body.text = textGroup
                    await send(body);
                }

                break;

            case "absence" :
                if (text.includes("@g.us")) {
                    var mytext = text.includes("@") ? text.split("@")[0] : text;
                    var group = await connection.query("select * from `group` where uid = ?", [mytext]);
                    var subject = "";
                    if (group.length) {
                        subject = group[0].title;
                    }
                    var textGroup = '';
                    const metadata = await waClients[token].groupMetadata(text);
                    if (metadata.subject) subject = metadata.subject;

                    textGroup += `
                *Group Details*
                Phone : ${metadata.id}
                Name: ${subject}
                Description: ${metadata.desc}
                Created At: ${metadata.creation}
                Created By: ${metadata.owner}
                Participant count: ${metadata.participants.length} Members
                `;
                    var participants = metadata.participants;
                    var obj = {} //store to obj
                    participants.forEach(function (item) {
                        var jid = item.jid.split("@s.whatsapp.net")[0];
                        var name = "";
                        if (item.hasOwnProperty("notify")) {
                            name = item.notify;
                        } else if (item.hasOwnProperty("vname")) {
                            name = item.vname;
                        }
                        obj[jid] = {
                            title: `${jid} ${name} `,
                            status: "Not Present"
                        }
                    });
                    const messages = await waClients[token].loadMessages(text, 100)
                    if (Array.isArray(messages)) {
                        console.log("array of messages");
                        messages.forEach(async(item) => {
                            var phone = item.participant.split("@s.whatsapp.net")[0];
                            if (obj.hasOwnProperty(phone)) {
                                obj[phone].status = "*Present*";
                            }
                        });
                    } else {
                        for (var key in messages) {
                            var amessage = messages[key];
                            if (Array.isArray(amessage)) {
                                amessage.forEach(function (item) {
                                    var newItem = item;
                                    if (typeof item === 'string') {
                                        newItem = JSON.parse(item);
                                    }
                                    var jid = newItem.participant.split("@")[0];
                                    if (obj.hasOwnProperty(jid)) {
                                        obj[jid].status = "*Present*";
                                    }
                                });
                            }
                        }
                    }
                    textGroup += "\r\n";
                    textGroup += `*Member Group :*`
                    textGroup += "\r\n";

                    var num = 1;
                    for (var key in obj) {
                        if (obj[key].status) {
                            textGroup += `${num}. ${obj[key].title}  ${obj[key].status}`;
                            textGroup += "\r\n";
                            num++;
                        }
                    }
                    body.type = "text";
                    body.text = textGroup
                    await send(body);
                }
                break;

            default :
                await waClients[token].sendMessage(to, text, MessageType.text);
                break;
        }


    } catch (err) {
        jsno = Util.jsonError("", err.toString());
    }

    return json;

}

module.exports = send;