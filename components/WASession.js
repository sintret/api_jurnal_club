/**
 * Created by sintret dev on 11/23/2021.
 */
const {
    default: makeWASocket,
    useSingleFileAuthState,
    DisconnectReason,
    getContentType
} = require('@adiwajshing/baileys')

var fs = require('fs')
var QRCode = require('qrcode');
var connection = require("./../models/database");
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');
var sendSocket = require("./sendSocket");
const P = require('pino')
const qrcode = require('qrcode-terminal')
const prefix = '.'
const ownerNumber = ['595995660558']
var myConfig = require("./../config/config");
const webp = require('webp-converter');
var API = require('./Api');
var WAConnect = require("./WAConnect");

var postData = (url, body) => {
    const options = {
        method: 'POST',
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        data: qs.stringify(body),
        url: url
    }
    axios(options).then(function (response) {
        //console.log(response);
    })
        .catch(function (error) {
            //console.log(error);
        });
    //var cb = await axios(options);
    console.log("send to " + url);
}


const WASession = async (req, res, next) => {
    if (waDoing == 1) {
        next();
        return;
    }
    var users = await connection.query("select * from `user`");
    var tokens = {}
    users.forEach(function (user) {
        tokens[user.token] = user;
    })
    var files = fs.readdirSync(dirRoot + "/public/sessions");
    console.log(files.length)
    if (files.length) {
        for (k in files) {
            var token = files[k].split(".")[0];
            if (!waClients[token]) {
                WAConnect(tokens[token]);
            }
        }
    }
    next();
}

module.exports = WASession;