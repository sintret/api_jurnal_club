/**
 * Created by sintret dev on 11/23/2021.
 */
const {WAConnection, MessageType} = require('@adiwajshing/baileys');

var fs = require('fs')
var QRCode = require('qrcode');
var connection = require("./../models/database");
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');
var sendSocket = require("./sendSocket");
var myConfig = require("./../config/config");
const webp=require('webp-converter');
var API = require('./Api');

var postData = (url, body) => {
    const options = {
        method: 'POST',
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        data: qs.stringify(body),
        url: url
    }
        axios(options).then(function (response) {
            //console.log(response);
        })
            .catch(function (error) {
                //console.log(error);
            });
        //var cb = await axios(options);
        console.log("send to " + url);

}

async function me(req, res, next) {
    var users = await connection.query("select * from `user` where wasession != ''");
    //console.log(users);
    users.forEach(async(userObj) => {
        var token = userObj.token;
        console.log(userObj.username + " : " + token);
        var sessionCfg = Util.isJsonString(userObj.wasession) ? JSON.parse(userObj.wasession) : "";
        if (sessionCfg) {
            var webhook = userObj.webhook;
            if (!waClients[token]) {
                waClients[token] = new WAConnection();
                // this will be called as soon as the credentials are updated
                waClients[token].loadAuthInfo(sessionCfg) // will load JSON credentials from file

                // called when WA sends chats
                // this can take up to a few minutes if you have thousands of chats!
                waClients[token].on('chats-received', async({hasNewChats}) => {
                    console.log(`you have ${waClients[token].chats.length} chats, new chats available: ${hasNewChats}`)
                    const unread = await waClients[token].loadAllUnreadMessages()
                    console.log("you have " + unread.length + " unread messages");
                    sendSocket.stats({
                        token: token,
                        type: "info",
                        message: "you have " + unread.length + " unread messages"
                    }, function (data) {
                        //console.log(data)
                    });
                });

                waClients[token].on('chat-update', async chatUpdate => {
                    // `chatUpdate` is a partial object, containing the updated properties of the chat
                    // received a new message
                    if (!chatUpdate.messages) return;

                    if (chatUpdate.messages && chatUpdate.count) {
                        const message = chatUpdate.messages.all()[0];
                        /*   const messageType = waClients[token].getMessageType(message) // get what type of message it is -- text, image, video

                         console.log("Type : "+messageType)*/
                        console.log("count")
                        console.log(message)
                        console.log("count")

                      /*  console.log("chats");
                        console.log(chatUpdate);*/
                        var isGroup = false;
                        var jid = '';
                        var phone = "";
                        if (chatUpdate.jid.includes("@s.whatsapp.net")) {
                            var explode = chatUpdate.jid.split("@s.whatsapp.net");
                            jid = explode[0] + "@c.us";
                            isGroup = false;
                            phone = explode[0];
                        } else {
                            jid = chatUpdate.jid;
                            isGroup = true;
                            phone = message.participant.split("@")[0];
                        }
                        console.log("phone : "+phone);
                        message.token = token;
                        message.from = userObj.phone;
                        console.log(webhook);
                        if (webhook) {
                            waMessages[phone] = message;
                            postData(webhook, message);
                        }

                        if (message.message.conversation) {
                            console.log("text");
                            if(message.message.conversation == "ping") {
                                console.log("ping");
                                await API({
                                    to:jid,
                                    type:"text",
                                    token:token,
                                    text : "pong",
                                    quoted : message
                                })
                            }

                        } else if(message.message.extendedTextMessage) {
                            /*console.log("Extended text");
                            console.log(typeof message.message.extendedTextMessage.contextInfo);
                            if(Array.isArray(message.message.extendedTextMessage.contextInfo)) {
                                console.log("context info is array")
                            } else {
                                console.log("context info is not array")
                            }
                            console.log(JSON.stringify(message.message.extendedTextMessage.contextInfo))*/
                        } else {
                            console.log("bukan text");
                        }

                        if (message.message.imageMessage) {
                            console.log("image message");
                            var caption = "";
                            if (message.message.imageMessage.caption) {
                                caption = message.message.imageMessage.caption;
                            }
                            var mimetype = message.message.imageMessage.mimetype;
                            console.log(mimetype);
                            console.log("caption :" + caption);

                            //save image and text caption to table image_story
                            if (caption.includes("!image_story")) {
                                var timestamp = Util.generateUnique(10);
                                var dir = dirRoot+"/public/downloads/";
                                var ext = mimetype.split("/")[1];
                                if(ext == "jpeg") ext = "jpg";
                                var filename = dir+timestamp;
                                const savedFilename = await waClients[token].downloadAndSaveMediaMessage(message, filename) // to decrypt & save to file
                                console.log(jid + " image story, saved at: " + savedFilename);

                               /* const stream = await waClients[token].downloadMediaMessage(message, "stream") // to decrypt & use as a buffer
                                stream.pipe(fs.createWriteStream(dir+filename))

*/
                                var dating = {
                                    text : caption.replace("!image_story ",""),
                                    image : savedFilename.split("downloads/")[1],
                                    image_count : 1,
                                    counting : 0
                                }
                                await  connection.query("insert into image_story set ?", dating);
                                await  API({
                                    to:jid,
                                    type:"text",
                                    token:token,
                                    text : "ok"
                                })

                               /* webp.grant_permission();
                                const result = webp.cwebp(savedFilename,filename+".webp","-q 80",logging="-v");
                                result.then( async (response) => {
                                    console.log(response);

                                    var url = "http://localhost:3004"
                                    if(myConfig.env == "production") {
                                        url = "https://api.jurnal.club"
                                    }
                                    await postData(url+"/send/sticker", {
                                        token: token,
                                        to : jid,
                                        url : url+"/downloads/"+timestamp+".webp"
                                    })
                                });*/
                            }
                        }


                    } else {
                        console.log("update")
                        console.log(chatUpdate)
                        console.log("update")

                    } // see updates (can be archived, pinned etc.)
                })


                /*  waClients[token].on ('message-new', async m => {
                 console.log("MESSAGE  NEW");
                 if (!m.message) return // if there is no text or media message
                 const messageType = Object.keys (m.message)[0]// get what type of message it is -- text, image, video
                 console.log(messageType)
                 // if the message is not a text message
                 if (messageType !== MessageType.text && messageType !== MessageType.extendedText) {
                 const buffer = await waClients[token].downloadMediaMessage(m) // to decrypt & use as a buffer

                 const savedFilename = await waClients[token].downloadAndSaveMediaMessage (m) // to decrypt & save to file
                 console.log(m.key.remoteJid + " sent media, saved at: " + savedFilename)
                 }
                 });*/


                waClients[token].on('CB:Conn,pushname', async json => {
                    console.log(JSON.stringify(json));
                    var wid = json[1].wid;
                    if (wid) {
                        console.log(wid);
                        var posting = {
                            phone: wid.split("@")[0],
                            properties: JSON.stringify(json)
                        }
                        //await connection.query("insert into `log` set ? ",posting);
                        await connection.query("update `user` set ? where token = ?", [posting, token]);
                    }
                })


                waClients[token].on('contacts-received', async() => {
                    //console.log(JSON.stringify(waClients[token].contacts));
                    //console.log('you have ' + Object.keys(waClients[token].contacts).length + ' contacts');
                    var contacts = waClients[token].contacts;
                    for (var key in contacts) {
                        if (key.includes("@s.whatsapp.net")) {
                            var name = "";
                            var phone = key.split("@s.whatsapp.net")[0];
                            if (contacts[key].hasOwnProperty("notify")) {
                                name = contacts[key].notify;
                            }
                            if (contacts[key].hasOwnProperty("vname")) {
                                name = contacts[key].vname;
                            }
                            if (name) {
                                var results = await connection.query("select id from phone where phone = ?", [phone]);
                                if (results.length) {
                                    var post = {
                                        name: name
                                    }
                                    await connection.query("update phone set ? where id = ?", [post, results[0].id]);
                                } else {
                                    var post = {
                                        name: name,
                                        phone: phone
                                    }
                                    await connection.query("insert into phone set ?", post);
                                }
                            }
                        }
                        //for group
                        if (key.includes("@g.us")) {
                            var name = "";
                            var phone = key.split("@g")[0];
                            if (contacts[key].hasOwnProperty("name")) {
                                name = contacts[key].name;
                            }
                            if (name) {
                                var post = {
                                    uid: phone,
                                    title: name
                                }
                                var results = await connection.query("select id from `group` where uid = ?", [phone]);
                                if (results.length) {
                                    await connection.query("update `group` set ? where uid = ?", [post, phone]);
                                } else {
                                    await connection.query("insert into `group` set ?", post);
                                }
                            }
                        }
                    }
                    console.log("update contact finish");
                })

                /*waClients[token].registerCallback (['action', null, 'user'], json => {
                 const userNode = json[2][0]
                 console.log ('got new user info, name: ' + userNode[1].notify + ', jid: ' + userNode[1].jid)
                 })*/

                await waClients[token].connect();
            }

        }
    });

    next();

}

module.exports = me;