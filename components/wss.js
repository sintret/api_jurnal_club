var config = require('./../config/config');
const WebSocket = require('ws');
const ws = new WebSocket(config.wss);

module.exports = ws;