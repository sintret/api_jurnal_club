const {WAConnection} = require('@adiwajshing/baileys');
var fs = require('fs')
var QRCode = require('qrcode');
var connection = require("./../models/database");
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');
var sendSocket = require("./sendSocket");

async function connectToWhatsApp(req, res) {
    var id = req.body.id;
    if (id) {
        var isReady = false;
        var results = await connection.query("select * from `user` where id = ?", [id]);
        var userObj = results[0];
        var token = userObj.token;
        if (!isReady) {
            if (!waClients[token]) {
                var conn = new WAConnection();
                // this will be called as soon as the credentials are updated
                conn.on('open', () => {
                    // save credentials whenever updated
                    console.log(`credentials updated!`);
                    console.log(JSON.stringify(conn.authInfo))

                    const authInfo = conn.base64EncodedAuthInfo() // get all the auth info we need to restore this session
                    /*console.log("authinfo")
                    console.log(JSON.stringify(authInfo))
                    console.log("authinfo")
                    console.log("WASecretBundle");
                    console.log(JSON.stringify(authInfo.WASecretBundle))
                    console.log(JSON.stringify(authInfo.WABrowserId))
                    console.log(JSON.stringify(authInfo.WAToken2))
                    console.log(JSON.stringify(authInfo.WAToken1))

                    console.log(typeof authInfo)*/

                    //fs.writeFileSync('./auth_info.json', JSON.stringify(authInfo, null, '\t')) // save this info to a file
                    /*
                     this.authInfo = {
                     clientID: authInfo.WABrowserId.replace(/\"/g, ''),
                     serverToken: authInfo.WAToken2.replace(/\"/g, ''),
                     clientToken: authInfo.WAToken1.replace(/\"/g, ''),
                     encKey: Buffer.from(secretBundle.encKey, 'base64'),
                     macKey: Buffer.from(secretBundle.macKey, 'base64'), // decode from base64
                     };
                     */
                    /*const secretBundle = typeof authInfo.WASecretBundle === 'string' ? JSON.parse(authInfo.WASecretBundle) : authInfo.WASecretBundle;

                    console.log(JSON.stringify(secretBundle));

                    const authInfoBrowser = {
                        WABrowserId: authInfo.WABrowserId.replace(/\"/g, ''),
                        WASecretBundle: JSON.stringify(authInfo.WASecretBundle),
                        WAToken1: authInfo.WAToken1.replace(/\"/g, ''),
                        WAToken2: authInfo.WAToken2.replace(/\"/g, '')
                    }
*/
                   // console.log(JSON.stringify(authInfoBrowser));
                    var data = {
                        wasession: JSON.stringify(authInfo, null, '\t'),
                        //wabrowser : JSON.stringify(authInfoBrowser, null, '\t')
                    }
                    connection.query("update `user` set ? where token = ?", [data, token]);
                    sendSocket.stats({token: token, type: "qrcodesuccess", message: "Ready"}, function (data) {
                        //console.log(data)
                    });
                    isReady = true;
                })

                // called when WA sends chats
                // this can take up to a few minutes if you have thousands of chats!
                conn.on('chats-received', async({hasNewChats}) => {
                    console.log(`you have ${conn.chats.length} chats, new chats available: ${hasNewChats}`)

                    const unread = await conn.loadAllUnreadMessages()
                    console.log("you have " + unread.length + " unread messages");
                    sendSocket.stats({
                        token: token,
                        type: "info",
                        message: "you have " + unread.length + " unread messages"
                    }, function (data) {
                        //console.log(data)
                    });
                })
                // called when WA sends chats
                // this can take up to a few minutes if you have thousands of contacts!
               /* conn.on('contacts-received', () => {
                    console.log('you have ' + Object.keys(conn.contacts).length + ' contacts')
                })*/

                if(!isReady) {
                    conn.on('qr', qr => {
                        // Now, use the 'qr' string to display in QR UI or send somewhere
                        console.log(qr);
                        QRCode.toDataURL(qr, async function (err, url) {
                            if (err) {
                                console.log("error", err);
                            }
                            console.log(url);
                            sendSocket.stats({token: token, type: "qrcode", message: url}, function (data) {
                                //console.log(data)
                            });
                        });
                    });
                }

                conn.on('chat-update', chatUpdate => {
                    // `chatUpdate` is a partial object, containing the updated properties of the chat
                    // received a new message
                    if (chatUpdate.messages && chatUpdate.count) {
                        const message = chatUpdate.messages.all()[0]
                        console.log(message)
                    } else console.log(chatUpdate) // see updates (can be archived, pinned etc.)
                })

                await conn.connect();
            }
        }
    }

    res.send("ok")
}

module.exports = connectToWhatsApp;