/*
terbaru 2022
from <String>
    - bisa dari group from : 120363023801393207@g.us
    - non group 6281575068530@s.whatsapp.net
isGroup<bool> true false;
sender : 6281575068530@s.whatsapp.net
senderNumber : 6281575068530
botNumber : 6281511880118
type : conversation
quoted : [object Object]
body : ini apa
 */
const {
    default: makeWASocket,
    useSingleFileAuthState,
    DisconnectReason,
    getContentType
} = require('@adiwajshing/baileys')

const fs = require('fs')
const P = require('pino')
const qrcode = require('qrcode-terminal')
const prefix = '.'
const ownerNumber = ['595995660558']

const connectToWA = async (req,res) => {

    const { state, saveState } = useSingleFileAuthState('./session.json')
    const conn = makeWASocket({
        logger: P({ level: 'silent' }),
        printQRInTerminal: true,
        auth: state,
    })

    conn.ev.on('connection.update', (update) => {
        const { connection, lastDisconnect } = update
        if (connection === 'close') {
            if (lastDisconnect.error.output.statusCode !== DisconnectReason.loggedOut) {
                connectToWA()
            }
        } else if (connection === 'open') {
            console.log('Bot Connected')
        }
    })

    conn.ev.on('creds.update', saveState)

    conn.ev.on('messages.upsert', async(mek) => {
        try {
            console.log(JSON.stringify(mek))
            mek = mek.messages[0]
            if (!mek.message) return

            mek.message = (getContentType(mek.message) === 'ephemeralMessage') ? mek.message.ephemeralMessage.message : mek.message
            if (mek.key && mek.key.remoteJid === 'status@broadcast') return
            const type = getContentType(mek.message)
            console.log(`type : ${type}`)
            const content = JSON.stringify(mek.message)
            const from = mek.key.remoteJid
            console.log(`from : ${from}`)

            const quoted = type == 'extendedTextMessage' && mek.message.extendedTextMessage.contextInfo != null ? mek.message.extendedTextMessage.contextInfo.quotedMessage || [] : []
            console.log(`quoted : ${quoted}`)

            const body = (type === 'conversation') ? mek.message.conversation : (type === 'extendedTextMessage') ? mek.message.extendedTextMessage.text : (type == 'imageMessage') && mek.message.imageMessage.caption ? mek.message.imageMessage.caption : (type == 'videoMessage') && mek.message.videoMessage.caption ? mek.message.videoMessage.caption : ''
            console.log(`body : ${JSON.stringify(body)}`)

            const isCmd = body.startsWith(prefix)
            const command = isCmd ? body.slice(prefix.length).trim().split(' ').shift().toLowerCase() : ''
            console.log(`command : ${command}`)

            const args = body.trim().split(/ +/).slice(1)
            const q = args.join(' ')
            const isGroup = from.endsWith('@g.us')
            const sender = mek.key.fromMe ? (conn.user.id.split(':')[0]+'@s.whatsapp.net' || conn.user.id) : (mek.key.participant || mek.key.remoteJid)
            const senderNumber = sender.split('@')[0]
            const botNumber = conn.user.id.split(':')[0]
            const pushname = mek.pushName || 'No Name'
            console.log(`pushname : ${pushname}`)
            const isMe = botNumber.includes(senderNumber)
            console.log(`isMe : ${isMe}`)

            const isOwner = ownerNumber.includes(senderNumber) || isMe
            console.log(`isOwner : ${isOwner}`)

            const reply = async(teks) => {
                await conn.sendMessage(from, { text: teks }, { quoted: mek })
            }

            switch (command) {

                case 'hola':
                    reply(`Hola ${pushname} como estas? :D`)
                    break

            }

        } catch (e) {
            const isError = String(e)

            console.log(isError)
        }
    })
}

module.exports = connectToWA;