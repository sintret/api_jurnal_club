var config = require('./../config/config');
var sendSocket = {}
const WebSocket = require('ws');


/*
 var data = {
 type: 'chat',
 token: token,
 me: me,
 msg: {
 to: to,
 text: text,
 uid: uid,
 cuid: custom_uid
 }
 }
 */

sendSocket.sendPromise = function (data) {
    return new Promise(function (resolve, reject) {
        const ws = new WebSocket(config.wss);
        ws.on('open', function open() {
            ws.send(JSON.stringify(data));
        });
        ws.on("message", function (msg) {
            ws.close();
            resolve(msg);
        });
    })
}

sendSocket.send = function (data, callback) {
    try {
        const ws = new WebSocket(config.wss);
        ws.on('open', function open() {
            ws.send(JSON.stringify(data));
        });
        ws.on("message", function (msg) {
            ws.close();
            callback(msg);
        });
    } catch (err) {
        console.log(err);
    }

}

sendSocket.stats = function (data, callback) {
    var url = config.wss+"/stats";
    console.log(url);
    try {
        const ws = new WebSocket(url);
        ws.on('open', function open() {
            ws.send(JSON.stringify(data));
        });
        ws.on("message", function (msg) {
            console.log(msg)
            ws.close();
            callback(msg);
        });
    } catch (err) {
        console.log(err);
    }

}

sendSocket.qrcode = function (data, callback) {
    const ws = new WebSocket(config.wss+"/qrcode");
    ws.on('open', function open() {
        ws.send(JSON.stringify(data));
    });
    ws.on("message", function (msg) {
        ws.close();
        callback(msg);
    });
}



sendSocket.contact = function (data, callback) {
    const ws = new WebSocket(config.wss);
    ws.on('open', function open() {
        ws.send(JSON.stringify(data));
    });
    ws.on("message", function (msg) {
        callback(msg)
        ws.close();
    });
}

//{firstname:'xxx',phone:'xxx'}
sendSocket.sendContact = function (data) {
    return new Promise(function (resolve, reject) {
        const ws = new WebSocket(config.wssContact);
        data.cmd = 'contact';
        ws.on('open', function open() {
            ws.send(JSON.stringify(data));
        });
        ws.on("message", function (msg) {
            resolve()
            ws.close();
        });
    })
}
module.exports = sendSocket