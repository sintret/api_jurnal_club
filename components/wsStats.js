/**
 * Created by sintret dev on 11/23/2021.
 */

var Util = require('./Util')
var stats = {}

stats.stats = async(ws,req) => {
    ws.on('message', function (msg) {
        console.log("/stats" + msg)
        var msgObj = Util.isJsonString(msg) ? JSON.parse(msg) : {};
        var token = msgObj.token || "";
        var register = !msgObj.register ? false : true;
        if(token){
            if(register) {
                webSocketClients[token] = ws;
            } else {
                try {
                    webSocketClients[token].send(msg);
                } catch (err) {
                    console.log(err);
                }
            }
        }
    });
}

stats.contact = async(ws,req) => {
        ws.on('message', function (msg) {
            var msgObj = JSON.parse(msg);
            var token = '';
            var cmd = msgObj.cmd;
            console.log("/contact" + msg)

            if(cmd == "contact"){
                msgObj.status = 1;
                for (var i = 0; i < clientsContact.length; i++) {
                    if(clientsContact[i].readyState == 1){
                        clientsContact[i].send(JSON.stringify(msgObj))
                    } else {
                        clientsContact.splice(i, 1);
                    }
                }

            } else {

                clientsContact.push(ws)
                var msgObj = {status:0, title:"ok"}
                for (var i = 0; i < clientsContact.length; i++) {
                    if(clientsContact[i].readyState == 1){
                        clientsContact[i].send(JSON.stringify(msgObj))
                    } else {
                        clientsContact.splice(i, 1);
                    }
                }

            }
        });
}

module.exports = stats;