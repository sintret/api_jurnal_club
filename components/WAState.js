var fs = require('fs')
var QRCode = require('qrcode');
var connection = require("./../models/database");
var Util = require('./Util');
var axios = require('axios');
var qs = require('qs');
var sendSocket = require("./sendSocket");
module.exports = async (req,res) => {
    var id = req.body.id;
    if (id) {
        var isReady = false;
        var results = await connection.query("select * from `user` where id = ?", [id]);
        var userObj = results[0];
        var token = userObj.token || null;
        console.log(token)
        if(token) {
            if(!waClients[token]) {
                require("./WAConnect")(userObj);
            } else {
                sendSocket.stats({token: token, type: "qrcodesuccess", message: "Ready"}, function (data) {
                    //console.log(data)
                });
                setTimeout(function () {
                    sendSocket.stats({token: token, type: "qrcodesuccess", message: "Ready"}, function (data) {
                        //console.log(data)
                    });
                },5000)
            }
        }
    }

    res.json("ok")
}