var env = process.env.NODE_ENV || 'development';

var config = {
    google :{
        clientID        : '386903282198-e9be0g62ibn3smov109fctmbftjn6hup.apps.googleusercontent.com',
        clientSecret    : 'fOzseZPgSODtkjV68E6nHhr0',
        callbackURL: env == 'development' ? 'http://localhost:3003/auth/google/callback' : 'https://jurnal.club/auth/google/callback',
        searchKey: 'AIzaSyAdzuxXUDlymnbVEP9bmsJvnezjRRF_Fq0',
        cx: '005431706121676160553:sdzxqxwdrcy',
        recaptchaSiteKey: '6LcLoX0UAAAAAGE-LiJO7sVhvmG6zz9OkdZB_0SH',
        recaptchaSecretKey : '6LcLoX0UAAAAADffijxuiwa68j5mOE3RD8w5wdB7',
        apicaptcha : '8ae5cb2f2b08490a026b1bc48cc17549'
    },
    facebook :{
        clientID        : '1089850394423080',
        clientSecret    : '6d9699d34205e7e64bd239d1ebd504aa',
        callbackURL: 'https://app.jurnal.club/auth/facebook/callback'
    },
    github :{
        clientID        : '1089850394423080',
        clientSecret    : '6d9699d34205e7e64bd239d1ebd504aa',
        callbackURL: 'https://app.jurnal.club/auth/github/callback'
    },
    line :{
        userId :'U1375404e3a756a8d61f2acc3e9d395dd',
        token : 'htpGYd3Vy4Rcpg9SYofJPbJ93IqV+nAVZjVVH3fkgC5rIqGGV4hrX88nUvcfgwQuFAz5Vhi28UrYUwi54Eyl+Ngw1OhiJ08LE9H4htxSqLBvndFoHgAj2wsnzofkWEYk2f3hSbMANMlhOG5GcU2uhgdB04t89/1O/w1cDnyilFU=',
        secret: 'fdb611c6dfa859be6fe6ccb1f99e2173',
        channelId: '1503333319'

    },
    ivone : {
        token : '9kFhOugM2vOstjNVMolU3w23YFtiJazE',
        uid: '6283839256468',
        captchaDir: 'D:/new_js/jurnal.club/public/captcha/',

    },
    darkSky: {
        api:'3e1777611679dd3c7084b0187e6cd354'
    },
    sendGrid: {
        apiKey: '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',
    },
    ojs : {
        email : "plugin@sintret.com"
    },
    wss: env == 'development' ? 'ws://localhost:3003' : 'wss://jurnal.club',
    urlScan: env == 'development' ? 'http://localhost:3021' : 'https://approval.aptiwise.com',
    apiUrl: env == 'development' ? 'http://localhost:3004' : 'https://api.jurnal.club',

    wssContact:'wss://jurnal.club/contact',
    chromeextensions:'https://chrome.google.com/webstore/detail/jurnal-club/cefknpdiadhhdehmhlmeoomdhihlhgcm',
    env:env
}

module.exports = config;