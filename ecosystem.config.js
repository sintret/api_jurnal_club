module.exports = {
  apps: [{
    name: 'api.jurnal.club',
    script: './bin/www',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    //args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '2G',
    env: {
      "PORT": 3004,
      "NODE_ENV": 'development'
    },
    env_production: {
      "PORT": 3004,
      "NODE_ENV": 'production'
    },
    env_testing: {
      "PORT": 3004,
      "NODE_ENV": 'testing'
    }
  }]
};
